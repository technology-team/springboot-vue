<%--
  Created by IntelliJ IDEA.
  User: ohs
  Date: 2017-01-10
  Time: 오후 5:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello Vue</title>
</head>
<body>
<div id="app">
    <h1>{{message}}</h1>
</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
<script>
    new Vue({
        el: '#app',
        data: {
            message: 'Greetings your Majesty!'
        }
    });
</script>
</html>
